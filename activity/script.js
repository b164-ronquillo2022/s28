//console.log("To God be the Glory!");


//sol 3 & 4

fetch(`https://jsonplaceholder.typicode.com/todos`)
.then(data => data.json())
.then(data => {
    let titlesArr = data.map(element => {
        return element.title
    })
    
    titlesArr.forEach(element => {
        console.log(element)
    });
    
})


//sol 5 & 6

fetch(`https://jsonplaceholder.typicode.com/todos/3`)
.then(data => data.json())
.then(data => {
    console.log(`Title: ${data.title} \n Status: ${data.status}`);
})


//sol 7

fetch(`https://jsonplaceholder.typicode.com/todos`, {
    method: "POST",
    headers: {"Content-type": "json/application"},
    body: JSON.stringify({
        userId: 1,
        title: "New Task to do via post",
        completed: false
    })
})
.then(data => data.json())
.then(data => {
    console.log(data);
})


//sol 8

fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PUT",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        userId: 4,
        title: "Updated post",
        completed: true
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})


//sol 9

fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PUT",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        title: "Updated Post",
        description: "Testing Description",
        status: "pending",
        dateCompleted: "",
        userId: 4,
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})


//sol 10

fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PATCH",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        title: "Updated Testing Description Patch",
        userId: 4
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})


//sol 11

fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PATCH",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        status: "completed",
        date: "2022-03-30",
        completed: true
    })
})
.then(res => res.json())
.then(res => {
    console.log(res)
})


//sol 12

fetch(`https://jsonplaceholder.typicode.com/todos/201`, {
    method: "DELETE"
})
