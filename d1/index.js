// console.log('hello');

//Javascript Synchronous and Asynchrounous

//Synchrounous Javascript
//Javascipt is by default synchrounous meaning that only one statment run/executed at atime

// console.log("Hello World");
// console.log("Hello Again");
// console.log("Goodbye");

// for(let i = 0; i <= 1000; i++){
// 	console.log(i);
// }

// //blocking is just a slow process of code
// console.log("Hello World")


// //Asynchronous Javascript

// function printMe(){
// 	console.log('print me');
// }

// function test(){
// 	console.log('test');
// }

// setTimeout(printMe, 5000);
// test();


//fetch is a method in JS, which allows us to send a request to an API and process its response
//the fetch receives a PROMISE
//A 'promise' is an object that represents
//fetch(url, {options}).then(response => response.json()).then(data => {console.log(data)})
//url = > the url to resource/routes from the Server
//optional objects = > it contains addtitional information about our request such as method, the body and the headers.
//it parse the response as JSON
//process the results

// console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// //A promise may be in one of 3 possible states: fulfilled

// fetch('https://jsonplaceholder.typicode.com/posts', {
// 	method:'GET'
// })
// .then(response => response.json())
// .then(data => {
// 	console.log(data)
// })

// //.then method captures the response object and returns another "promise"
// //which eventually be "resolved" or "rejected"
// //"async" and "await" keywords
// function fetchData(){
//      let result = await fetch('https://jsonplaceholder.typicode.com/posts');

//      //result returned by fetch
//      console.log(result);

//      console.log(typeof result);
//      console.log(result.body);
//      //converts data from the response

//      let json = await result.json();

//      console.log(json);
// }

// fetchData();

// //Creating a post
// fetch('https://jsonplaceholder.typicode.com/posts',{
// 	method: 'POST',
// 	headers: {
// 		'Content-Type': 'application/json'
// 	},
// 	body: JSON.stringify({
// 		title: 'New post',
// 		body:'Hello World',
// 		userId: 1
// 	})
// });
// .then(response => response.json())
// .then(json => console.log(json))


// //Updating a post
// //update a specific post following the rest API (update, PUT)

// fetch('https://jsonplaceholder.typicode.com/posts/1',{
// 	method: 'PUT',
// 	headers: {
// 		'Content-Type': 'application/json'
// 	},
// 	body:JSON.stringify({
// 		id: 1,
// 		title: "Updated post",
// 		body: 
// 	})
// })







//
//====================================================================================================================================================================================================================================





//Javascript Synchronous vs Asynchronous

//Synchronous Javascript
//Javascript is by default is synchronous meaning that only one statement run/executed at a time

// console.log("Hello World");

// console.log("Goodbye");

// for(let i = 0; i <= 1000; i++){
// 	console.log(i);
// }

//blocking, is just a slow process of code.
//console.log("Hello World")


//Asynchronous Javascript
//Asynchronous communication is a method of exchanging messages between two or more parties in which each party receives and processes messages whenever it's convenient or possible to do so, rather than doing so immediately upon receipt.

// function printMe() {
// 	console.log('print me');
// }

// function test(){
// 	console.log('test');
// }

// setTimeout(printMe, 5000);
// test();

//The Fetch API allows you to asynchronously request for a resource (data)
//the fetch receives a PROMISE
//A 'Promise' is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

//fetch() is a method in JS, which allows us to send a request to an API and process its response

//syntax:

//fetch(url, {options}).then(response => response.json()).then(data => {console.log(data)})
//url = > the url to resource/routes from the Server
//optional objects = >it contains additional information about our requests such as method, the body and the headers.

//it parse the response as JSON
//process the results
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

//A promise may be in one of 3 possible states: fulfilled, rejected or pending




//Retrieves all posts (GET)

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => response.json())
.then(data => {
	console.log(data)
})
//.then method captures the response object and returns another "promise" which will eventually be "resolved" or "rejected"

//"async" and "await" keywords is another approach that can be used to achieve asynchronous code
//used in functions to indicate which portions of code should be waited for 

async function fetchData(){

	let result = await fetch('https://jsonplaceholder.typicode.com/posts');

	//Result returned by fetch 
	console.log(result);

	console.log(typeof result);

	console.log(result.body);

	//Converts data from the response object as JSON
	let json = await result.json();

	console.log(json);

}

fetchData()


//Creating a post
//Create a new post following the Rest API(create, POST)
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello World',
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))



//Updating a post

//update a specific post following the Rest API (update, PUT)
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated post",
		body: "Hello again",
		userId: 1
	})
})
.then(res => res.json())
.then(data => console.log(data))


//Patch method
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated post"
	})
})
.then(res => res.json())
.then(data => console.log(data))

//The PUT is a method modifying resources where the client sends data that updates the ENTIRE resources. It is used to set an entity's information completely
//Patch method applies a partial update to the resources

//Delete a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))


//Filtering posts
//The data can be filtered by sending the userId along with the URL
//? symbol at the end of the URL indicates that parameters will be sent to the endpoint
//Syntax:
//Individual Parameters
	//'url?parameterName=value'
//Multiple Parameters
	//'url?parameterNameA=valueA&paramB=valueB'


fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(res => res.json())
.then(data => console.log(data))




//Retrieving nested/related data

fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then(res => res.json())
.then(data => console.log(data))



















